import { Component, OnInit } from '@angular/core';
import { JeopardyService } from '../jeopardy.service';
import { Category } from '../types/category';

@Component({
  selector: 'app-browse-category-list',
  templateUrl: './browse-category-list.component.html',
  styleUrls: ['./browse-category-list.component.scss']
})
export class BrowseCategoryListComponent implements OnInit {

  categories: Category[];

  offset = 0;
  amount = 50;

  previousEnabled = false;
  nextEnabled = true;

  constructor(private jeopardyService: JeopardyService) { }

  ngOnInit(): void {
    this.getCategories();
  }

  getCategories(): void {
    this.jeopardyService.getCategories(this.amount, this.offset).subscribe(list => 
      {
        this.categories = list;
        this.previousEnabled = (this.offset > 0);
        this.nextEnabled = (this.categories.length < this.amount);
      });
  }

  onNext(): void {
    this.offset += this.amount;
    this.getCategories();
  }

  onPrevious(): void {
    this.offset -= this.amount;
    if(this.offset < 0) {
      this.offset = 0;
    }

    this.getCategories();
  }

}
