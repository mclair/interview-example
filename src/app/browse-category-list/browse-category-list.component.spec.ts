import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BrowseCategoryListComponent } from './browse-category-list.component';

describe('BrowseCategoryListComponent', () => {
  let component: BrowseCategoryListComponent;
  let fixture: ComponentFixture<BrowseCategoryListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BrowseCategoryListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BrowseCategoryListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
