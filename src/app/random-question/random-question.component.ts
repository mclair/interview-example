import { Component, OnInit } from '@angular/core';
import { JeopardyService } from '../jeopardy.service';
import { Clue } from '../types/clue';

@Component({
  selector: 'app-random-question',
  templateUrl: './random-question.component.html',
  styleUrls: ['./random-question.component.scss']
})
export class RandomQuestionComponent implements OnInit {

  clue: Clue;

  constructor(private jeopardyService: JeopardyService) { }

  ngOnInit(): void {
    this.getQuestion();
  }

  getQuestion(): void {
    this.jeopardyService.getRandomQuestion().subscribe(clue => {
      this.clue = clue[0];
      console.log(clue);
    });
  }

  onChangeQuestion(): void {
    this.getQuestion();
  }
}
