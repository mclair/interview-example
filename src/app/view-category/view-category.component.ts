import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JeopardyService } from '../jeopardy.service';
import { Category } from '../types/category';
import { Clue } from '../types/clue';

import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { ViewAnswerModalComponent } from '../view-answer-modal/view-answer-modal.component';


@Component({
  selector: 'app-view-category',
  templateUrl: './view-category.component.html',
  styleUrls: ['./view-category.component.scss']
})
export class ViewCategoryComponent implements OnInit {

  id: number;
  category: Category

  constructor(private route: ActivatedRoute, private jeopardyService: JeopardyService, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.id = params['id'];
      console.log(this.id);
      this.jeopardyService.getCategoryClues(this.id).subscribe(cat => this.category = cat);
    });
  }

  viewAnswer(clue: Clue):void {
    const modalRef = this.modalService.open(ViewAnswerModalComponent);
    modalRef.componentInstance.clue = clue;
  }
}
