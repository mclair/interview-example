import { Component, Input, OnInit } from '@angular/core';
import { Clue } from '../types/clue';

import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-view-answer-modal',
  templateUrl: './view-answer-modal.component.html',
  styleUrls: ['./view-answer-modal.component.scss']
})
export class ViewAnswerModalComponent implements OnInit {

  @Input()
  clue: Clue;

  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit(): void {
  }

}
