import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewAnswerModalComponent } from './view-answer-modal.component';

describe('ViewAnswerModalComponent', () => {
  let component: ViewAnswerModalComponent;
  let fixture: ComponentFixture<ViewAnswerModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewAnswerModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewAnswerModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
