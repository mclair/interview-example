import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Clue } from './types/clue';
import { Category } from './types/category';

@Injectable({
  providedIn: 'root'
})
export class JeopardyService {
  private rootApi = 'https://jservice.io/api';

  constructor(private http:  HttpClient) { }

  getRandomQuestion(): Observable<Clue[]> {
    return this.http.get<Clue[]>(`${this.rootApi}/random`);
  }

  getCategories(amount: number, offset: number) {
    return this.http.get<Category[]>(`${this.rootApi}/categories?count=${amount}&offset=${offset}`);
  }

  getCategoryClues(id: number) {
    return this.http.get<Category>(`${this.rootApi}/category?id=${id}`);
  }
}
