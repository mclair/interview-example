import { Component, Input, OnInit } from '@angular/core';
import { Clue } from '../types/clue';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss']
})
export class QuestionComponent implements OnInit {
  @Input()
  clue: Clue;

  constructor() { }

  ngOnInit(): void {
  }

}
