import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BrowseCategoryListComponent } from './browse-category-list/browse-category-list.component';
import { BrowseRootComponent } from './browse-root/browse-root.component';
import { LandingComponent } from './landing/landing.component';
import { PlayRootComponent } from './play-root/play-root.component';
import { RandomQuestionComponent } from './random-question/random-question.component';
import { ViewCategoryComponent } from './view-category/view-category.component';

const routes: Routes = [
  { path: '', component: LandingComponent },
  { path: 'browse', component: BrowseRootComponent },
  { path: 'play', component: PlayRootComponent },
  { path: 'browse/random', component: RandomQuestionComponent },
  { path: 'browse/categories', component: BrowseCategoryListComponent },
  { path: 'category/:id', component: ViewCategoryComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
