import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayRootComponent } from './play-root.component';

describe('PlayRootComponent', () => {
  let component: PlayRootComponent;
  let fixture: ComponentFixture<PlayRootComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlayRootComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayRootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
