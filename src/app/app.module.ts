import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RandomQuestionComponent } from './random-question/random-question.component';
import { QuestionComponent } from './question/question.component';
import { BrowseRootComponent } from './browse-root/browse-root.component';
import { PlayRootComponent } from './play-root/play-root.component';
import { LandingComponent } from './landing/landing.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowseCategoryListComponent } from './browse-category-list/browse-category-list.component';
import { ViewCategoryComponent } from './view-category/view-category.component';
import { ViewAnswerModalComponent } from './view-answer-modal/view-answer-modal.component';

@NgModule({
  declarations: [
    AppComponent,
    RandomQuestionComponent,
    QuestionComponent,
    BrowseRootComponent,
    PlayRootComponent,
    LandingComponent,
    BrowseCategoryListComponent,
    ViewCategoryComponent,
    ViewAnswerModalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
