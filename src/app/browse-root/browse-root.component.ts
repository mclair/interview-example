import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-browse-root',
  templateUrl: './browse-root.component.html',
  styleUrls: ['./browse-root.component.scss']
})
export class BrowseRootComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

}
